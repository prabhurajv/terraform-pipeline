provider "azurerm" {
    subscription_id = "3ab8f508-6bc5-40ec-8ef5-af01563b8977"
    client_id       = "5b51ad34-a095-4627-ae1c-ec7b8e9532a6"
    client_secret   = "ee7bc8e0-aad5-4870-90a6-0e6d01a2af64"
    tenant_id       = "edf442f5-b994-4c86-a131-b42b03a16c95"
}

variable "resource_grp_name" {
  type = "string"
}
variable "vnet_cidr" {
  type = "list"
}
variable "subnet_cidr" {
  type = "string"
}
variable "version" {
  type = "string"
}
variable "environment" {
  type = "string"
}
variable "CostCenter" {
  type = "string"
}
variable "Application" {
  type = "string"
}
variable "Purpose" {
  type = "string"
}


# Create a resource group
resource "azurerm_resource_group" "IBM_MFP" {
  name = "${var.resource_grp_name}"
  location = "East US"
  tags {
    environment = "${var.environment}"
	CostCenter = "${var.CostCenter}"
	Application = "${var.Application}"
    Purpose = "${var.Purpose}"
  }
}

resource "azurerm_virtual_network" "IBM_MFP" {
  name = "IBM_MFP_Vnet1"
  resource_group_name = "${azurerm_resource_group.IBM_MFP.name}"
  location = "${azurerm_resource_group.IBM_MFP.location}"
  address_space = "${var.vnet_cidr}"
  tags {
    environment = "${var.environment}"
	CostCenter = "${var.CostCenter}"
	Application = "${var.Application}"
    Purpose = "${var.Purpose}"
  }
}

resource "azurerm_subnet" "IBM_MFP_SUBNET" {
  name = "IBM_MFP_SUBNET1"
  resource_group_name  = "${azurerm_resource_group.IBM_MFP.name}"
  virtual_network_name = "${azurerm_virtual_network.IBM_MFP.name}"
  address_prefix = "${var.subnet_cidr}"
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "IBM_MFP_SG" {
    name = "IBM_MFP_NSG1"
    location = "eastus"
    resource_group_name = "${azurerm_resource_group.IBM_MFP.name}"

    security_rule {
        name = "SSH"
        priority = 1001
        direction = "Inbound"
        access = "Allow"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "22"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name = "Port_81"
        priority = 1002
        direction = "Inbound"
        access = "Allow"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "81"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name = "Port_8081"
        priority = 1003
        direction = "Inbound"
        access = "Allow"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "8081"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }

    tags {
    environment = "${var.environment}"
	CostCenter = "${var.CostCenter}"
	Application = "${var.Application}"
    Purpose = "${var.Purpose}"
  }
}

resource "azurerm_network_interface" "IBM_MFP" {
  name = "IBM_MFP-NIC1"
  location = "${azurerm_resource_group.IBM_MFP.location}"
  resource_group_name = "${azurerm_resource_group.IBM_MFP.name}"
  network_security_group_id = "${azurerm_network_security_group.IBM_MFP_SG.id}"
  
  ip_configuration {
    name = "IBM_MFP_NIC1"
    subnet_id = "${azurerm_subnet.IBM_MFP_SUBNET.id}"
    private_ip_address_allocation = "Dynamic"
	public_ip_address_id = "${azurerm_public_ip.IBM_MFP_PUBLIC_IP.id}"
  }
  tags {
    environment = "${var.environment}"
	CostCenter = "${var.CostCenter}"
	Application = "${var.Application}"
    Purpose = "${var.Purpose}"
  }
}

resource "azurerm_public_ip" "IBM_MFP_PUBLIC_IP" {
  name = "PublicIp1"
  location = "East US"
  resource_group_name = "${azurerm_resource_group.IBM_MFP.name}"
  allocation_method = "Dynamic"

  tags {
		environment = "${var.environment}"
        CostCenter = "${var.CostCenter}"
        Application = "${var.Application}"
        Purpose = "${var.Purpose}"
  }
}

resource "azurerm_virtual_machine" "IBM_MFP" {
  name = "IBM_MFP_VM1"
  location = "${azurerm_resource_group.IBM_MFP.location}"
  resource_group_name   = "${azurerm_resource_group.IBM_MFP.name}"
  network_interface_ids = ["${azurerm_network_interface.IBM_MFP.id}"]
  vm_size = "Standard_DS1_v2"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
   delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
   delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "RedHat"
    offer = "RHEL"
    sku = "7-RAW"
    version = "${var.version}"
  }
  storage_os_disk {
    name = "IBM_MFP_ROOT"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name = "IBMMFP1"
    admin_username = "vmadmin"
    admin_password = "Pa55w0rd@123"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  tags {
    environment = "${var.environment}"
	CostCenter = "${var.CostCenter}"
	Application = "${var.Application}"
    Purpose = "${var.Purpose}"
  }
}
